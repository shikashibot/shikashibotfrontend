export class ShikashiConfig {
  // Tht title of the app
  APP_TITLE               = 'ShikashiBot';
  // The URL of the shikashi backend API
  SHIKASHI_API_BASE_URL   = 'http://localhost:5000/api/';
  // The ID of the app registered at twitch
  TWITCH_API_CLIENT_ID    = 'h8xqbb63mtlgy8kkxv8wrmb0sg1z1h3';
  // The URL the user get refirected to after the twich log in.
  TWITCH_API_REDIRECT_URI = 'http://localhost:5555';
}
