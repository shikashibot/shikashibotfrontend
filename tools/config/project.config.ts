import {join} from 'path';
import {SeedConfig} from './seed.config';
import {ShikashiConfig} from './shikashi.config';
import {InjectableDependency} from './seed.config.interfaces';

export class ProjectConfig extends SeedConfig {
  PROJECT_TASKS_DIR = join(process.cwd(), this.TOOLS_DIR, 'tasks', 'project');

  FONTS_DEST = `${this.APP_DEST}/fonts`;
  FONTS_SRC = [
    'node_modules/bootstrap/dist/fonts/**'
  ];

  shikashiConfig: ShikashiConfig;

  constructor() {
    super();

    this.shikashiConfig = new ShikashiConfig();

    let additional_deps: InjectableDependency[] = [
      {src: 'jquery/dist/jquery.min.js', inject: 'libs'},
      {src: 'bootstrap/dist/js/bootstrap.min.js', inject: 'libs'},
      {src: 'bootswatch/slate/bootstrap.min.css', inject: true},
      {src: 'chart.js/dist/Chart.bundle.min.js', inject: 'libs'}
      // {src: 'lodash/lodash.min.js', inject: 'libs'},
    ];

  /*let additional_deps: InjectableDependency[] = [
    { src: 'jquery/dist/jquery.min.js', inject: 'libs' },
    { src: 'bootstrap/dist/js/bootstrap.min.js', inject: 'libs' },
    { src: 'bootswatch/slate/bootstrap.min.css', inject: true },
    { src: `../src/assets/main.css`, inject: true },
    { src: `../src/assets/toggle.css`, inject: true },
    { src: `../src/assets/spinner.css`, inject: true }
    // {src: 'lodash/lodash.min.js', inject: 'libs'},
    ];*/


    const seedDependencies = this.NPM_DEPENDENCIES;

    this.NPM_DEPENDENCIES = seedDependencies.concat(additional_deps);

    this.APP_ASSETS = [
      { src: `${this.ASSETS_SRC}/main.css`, inject: true },
      { src: `${this.ASSETS_SRC}/toggle.css`, inject: true },
      { src: `${this.ASSETS_SRC}/spinner.css`, inject: true }
    ];
  }
}
