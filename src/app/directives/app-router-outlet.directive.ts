import {RouterOutlet, Router, ComponentInstruction} from '@angular/router-deprecated';
import {Directive, DynamicComponentLoader, Input, OnInit, ViewContainerRef} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {AlertService} from '../../app/services/alert.service';
import {AppRouterService} from '../../app/services/app-router.service';
import {LandingService} from '../../landing/services/landing.service';

@Directive({
  selector: 'app-router-outlet'
})

/**
 * This directive is responsible for the routing between the landing and the main page.
 * If the user is logged in it redirects automatically to the main page.
 * If a twitch token is provided in the URL it obtains a shikashi token from the server.
 */
export class AppRouterOutlet extends RouterOutlet implements OnInit {

  constructor(viewContainterRef: ViewContainerRef, dynamicComponentLoader: DynamicComponentLoader, private router: Router, @Input() nameAttr: string, private authService: AuthService, private alertService: AlertService, private appRouterService: AppRouterService, private landingService: LandingService) {
    super(viewContainterRef, dynamicComponentLoader, router, nameAttr);
  }

  ngOnInit():any {
    this.appRouterService.setRouter(this.router);
  }

  activate(nextInstruction: ComponentInstruction): Promise<any> {
    const url = nextInstruction.urlPath;
    const credentials = this.authService.getShikashiCredentials();
    const twitchToken = nextInstruction.params['code'];

    if (url === '' ) {

      if (credentials && credentials.channelName && credentials.token) {
        this.router.navigate(['Main']);
      } else if (twitchToken) {
        this.landingService.setIsLoggingIn(true);
        this.authService.obtainAndStoreShikashiToken(twitchToken)
          .subscribe((success:any) => {
            if (success) {
              this.router.navigate(['Main']);
              this.landingService.setIsLoggingIn(false);
            } else {
              this.alertService.alert({type: 'alert-danger', message: 'Authorization failed.', autoResolve: true});
              this.landingService.setIsLoggingIn(false);
            }
          });
      }

    }

    return super.activate(nextInstruction);
  }
}
