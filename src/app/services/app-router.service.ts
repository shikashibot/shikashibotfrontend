import {Injectable} from '@angular/core';
import {Router} from '@angular/router-deprecated';

@Injectable()

/**
 * This service holds a reference to the appRouterOutlet.
 * The reference is set by the appRouterOutlet itself and used by the authErrorService.
 */
export class AppRouterService {

  private router: Router;

  setRouter(router: Router) {
    this.router = router;
  }

  getRouter() {
    return this.router;
  }

}
