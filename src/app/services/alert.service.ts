import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/Rx';

@Injectable()

/**
 * This service is can be used by all components throughout the app to communicate
 * with the AlertComponent to show notifications to the user.
 */
export class AlertService {

  private showAlert: EventEmitter<any> = new EventEmitter();
  private resolveAlert: EventEmitter<any> = new EventEmitter();

  /**
   * Returns the showAlert event emitter the AlertComponent listens to.
   * @return showAlert
   */
  showAlertEvent() {
    return this.showAlert;
  }

  /**
   * Returns the resolveAlert event emitter the AlertComponent listens to.
   * @return showAlert
   */
  resolveAlertEvent() {
    return this.resolveAlert;
  }

  /**
   * Emits the showAlert event.
   * @param alertMessage An object holding "type", "message" and "autoResolve".
   * @param type A string which equals the bootstrap classes alert-success, alert-info, alert-warning or alert-danger.
   * @param message A string which is presented to the user.
   * @param autoResolve A flag indicating if the notification should disappear automatically after some time.
   * @return showAlert The event is emitted and carries the alertMessage object.
   */
  alert(alertMessage:any) {
    this.showAlert.emit(alertMessage);
  }

  /**
   * Emits the resolveAlert event.
   * @return showAlert
   */
  resolve() {
    this.resolveAlert.emit(null);
  }
}
