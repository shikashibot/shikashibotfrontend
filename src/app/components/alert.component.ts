import {Component, OnInit} from '@angular/core';
import {AlertService} from '../../app/services/alert.service';

@Component({
  selector: 'sb-alert',
  moduleId: module.id,
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})

/**
 * This component listens to the events of alertService and accordingly shows/hides the alert-notifications
 */
export class AlertComponent implements OnInit {

  alertMessage: any = null;

  constructor(private alertService: AlertService) {}

  ngOnInit(): any {

    this.alertService.showAlertEvent()
      .subscribe(
        (alertMessage:any) => this.showAlert(alertMessage)
      );

    this.alertService.resolveAlertEvent()
      .subscribe(
        () => this.hideAlert()
      );

  }

  showAlert(alertMessage: any) {
    this.alertMessage = alertMessage;
    if(alertMessage.autoResolve) {
      window.setTimeout(() => {
        this.alertMessage = null;
      }, 3500);
    }
  }

  hideAlert() {
    this.alertMessage = null;
  }

}
