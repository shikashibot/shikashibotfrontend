import {Component, OnInit} from '@angular/core';
import {RouteConfig, Router} from '@angular/router-deprecated';
import {LandingComponent} from '../../landing/components/landing.component';
import {MainComponent} from '../../main/components/main.component';
import {AppRouterOutlet} from '../directives/app-router-outlet.directive';
import {AuthService} from '../../shared/services/auth.service';
import {AlertComponent} from './alert.component';
import {NotFoundComponent} from '../../shared/components/NotFound.component';
import {AlertService} from '../services/alert.service';

@Component({
  selector: 'sb-app',
  moduleId: module.id,
  templateUrl: './app.component.html',
  directives: [AppRouterOutlet, AlertComponent]
})

@RouteConfig([
  { path: '/', name: 'Landing', component: LandingComponent, useAsDefault: true },
  { path: '/main/...', name: 'Main', component: MainComponent },
  { path: '/404', name: '404', component: NotFoundComponent },
  { path: '/*path', redirectTo:['404'] }
])

/**
 * This component is the base component. It includes the alert-component and the app-router-outlet
 * (routing between 'Landing' and 'Main'). It uses authService to check if a valid shikashi-token
 * exists.
 */
export class AppComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private alertService: AlertService) {}

  ngOnInit() {
    // ServiceWorker
    if ('serviceWorker' in navigator) {
      (<any>navigator).serviceWorker.register('./sw.js').then(function(registration:any) {
        console.log('ServiceWorker registration successful with scope: ',    registration.scope);
      }).catch(function(err:any) {
        console.log('ServiceWorker registration failed: ', err);
      });

      if (navigator.serviceWorker.controller) {
        var messageChannel = new MessageChannel();
        navigator.serviceWorker.controller.postMessage({ command: 'wasUpdated' }, [messageChannel.port2]);

        messageChannel.port1.onmessage = (event) => {
          if (event.data.wasUpdated) {
            this.alertService.alert({type: 'alert-info', message: 'Parts of the page were updated. Please <a href=".">reload the page.</a>', autoResolve: false});
          }
        };
      }
    }
    // Application startup token validation
    this.authService.validateShikashiToken()
      .subscribe((success:any) => {
        if (!success) {
          this.authService.invalidateShikashiToken();
          this.router.navigate(['Landing']);
        }
      });
  }
}
