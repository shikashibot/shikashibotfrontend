import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/Rx';
import {HttpService} from '../../shared/services/http.service';

@Injectable()

/**
 * This service uses the HttpService to receive statistics from the server
 */
export class StatisticsService {

  statisticsEndpoint = 'channels/[CHANNELNAME]/statistics/channel/';

  constructor (private httpService: HttpService) {}

  /**
   * Gets the recent statistics data.
   * @return getRecentStatistics This event is fired as soon as a response is received. Carries either the statistics data
   * or null (in case of failure)
   */
  getRecentStatistics() {
    var getRecentStatistics = new EventEmitter();

    this.httpService.get(this.statisticsEndpoint + 'recent')
      .map(response => response.json())
      .subscribe(
        data => {
          console.log('Successfully received recent statistics.');
          getRecentStatistics.emit(data);
        },
        error => {
          console.log('Could not load recent statistics.');
          getRecentStatistics.emit(null);
        }
      );

    return getRecentStatistics;
  }

  /**
   * Gets the weekly statistics data.
   * @return getWeeklyStatistics This event is fired as soon as a response is received. ICarries either the statistics data
   * or null (in case of failure)
   */
  getWeeklyStatistics() {
    var getWeeklyStatistics = new EventEmitter();

    this.httpService.get(this.statisticsEndpoint + 'weekly')
      .map(response => response.json())
      .subscribe(
        data => {
          console.log('Successfully received weekly statistics.');
          getWeeklyStatistics.emit(data);
        },
        error => {
          console.log('Could not load weekly statistics.');
          getWeeklyStatistics.emit(null);
        }
      );

    return getWeeklyStatistics;
  }

}
