import {Component} from '@angular/core';
import {ChartsStatisticsComponent} from './charts-statistics.component';
import {StatisticsService} from '../../services/statistics.service';

@Component({
  selector: 'sb-statistics',
  moduleId: module.id,
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css'],
  directives: [ChartsStatisticsComponent],
  providers: [StatisticsService]
})

export class StatisticsComponent {

  activePage = 'recent';

  setActivePage(activePage: string) {
    this.activePage = activePage;
  }
}
