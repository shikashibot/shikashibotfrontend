import {Component, OnInit, Input} from '@angular/core';
import {AlertService} from '../../../app/services/alert.service';
import {StatisticsService} from '../../services/statistics.service';
import {CHART_DIRECTIVES} from 'ng2-charts/ng2-charts';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass} from '@angular/common';

@Component({
  selector: 'sb-charts-statistics',
  moduleId: module.id,
  templateUrl: './charts-statistics.component.html',
  styleUrls: ['./charts-statistics.component.css'],
  directives: [CHART_DIRECTIVES, NgClass, CORE_DIRECTIVES, FORM_DIRECTIVES]
})

export class ChartsStatisticsComponent implements OnInit {

  private loadingComplete: boolean;

  private colors = [
    { // black
      backgroundColor: 'rgba(0,0,0,0.2)',
      borderColor: 'rgba(0,0,0,1)',
      pointBackgroundColor: 'rgba(0,0,0,1)',
      pointBorderColor: '#ccc',
      pointHoverBackgroundColor: '#ccc',
      pointHoverBorderColor: 'rgba(0,0,0,1)'
    },
    /*{ // red
      backgroundColor: 'rgba(187,30,30,0.2)',
      borderColor: 'rgba(187,30,30,1)',
      pointBackgroundColor: 'rgba(187,30,30,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(187,30,30,0.8)'
    },*/
    { // blue
      backgroundColor: 'rgba(37,40,140,0.2)',
      borderColor: 'rgba(37,40,140,1)',
      pointBackgroundColor: 'rgba(37,40,140,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(37,40,140,0.8)'
    }
  ];

  // penaltyLineChart
  public penaltyLineChartData:Array<any>;
  public penaltyLineChartLabels:Array<any>;
  public penaltyLineChartOptions:any = {
    animation: false,
    responsive: true
  };
  public penaltyLineChartColours:Array<any> = this.colors;
  public penaltyLineChartLegend:boolean = true;
  public penaltyLineChartType:string = 'line';

  // usageLineChart
  public usageLineChartData:Array<any>;
  public usageLineChartLabels:Array<any>;
  public usageLineChartOptions:any = {
    animation: false,
    responsive: true
  };
  public usageLineChartColours:Array<any> = this.colors;
  public usageLineChartLegend:boolean = true;
  public usageLineChartType:string = 'line';

  @Input() type: string;

  constructor(private statisticsService: StatisticsService, private alertService: AlertService) {}

  ngOnInit() {
    this.loadStatistics();
  }

  /**
   * Uses the statisticsService to load the statistics from the server.
   */
  loadStatistics() {
    var loadingStatistics = this.type === 'weekly' ? this.statisticsService.getWeeklyStatistics() : this.statisticsService.getRecentStatistics();
    loadingStatistics
      .subscribe((data:any) => {
        if (data) {
          this.prepareStatisticsData(data);
          this.loadingComplete = true;
        } else {
          this.alertService.alert({type: 'alert-danger', message: 'Could not load statistics.', autoResolve: true});
        }
      });
  }

  /**
   * Prepares the data so it can be used by the ng-charts directive.
   * @param data Data as received from the server
   */
  prepareStatisticsData(data:any) {
    var preparedData = {banCount: Array<Number>(), messageCount: Array<Number>(), timeoutCount: Array<Number>(), timestamps: Array<any>(), viewCount: Array<Number>()};

    for (var i = 0; i<data.length; i++) {
      preparedData.banCount.push(data[i].banCount);
      preparedData.messageCount.push(data[i].messageCount);
      preparedData.timeoutCount.push(data[i].timeoutCount);
      preparedData.timestamps.push(this.type === 'weekly' ? data[i].timestamp.substring(0,10) : data[i].timestamp.substring(11,16));
      preparedData.viewCount.push(data[i].viewCount);
    }

    this.penaltyLineChartData = [
      {data: preparedData.banCount, label: 'Bans'},
      {data: preparedData.timeoutCount, label: 'Timeouts'},
    ];

    this.penaltyLineChartLabels = preparedData.timestamps;

    this.usageLineChartData = [
      {data: preparedData.viewCount, label: 'Viewers'},
      {data: preparedData.messageCount, label: 'Messages'}
    ];

    this.usageLineChartLabels = preparedData.timestamps;
  }

}
