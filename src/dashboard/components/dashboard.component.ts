import {Component, OnInit, AfterViewInit} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {StatisticsComponent} from './statistics/statistics.component';
import {Chat} from './chat/chat';

@Component({
  selector: 'sb-dashboard',
  moduleId: module.id,
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css', './chat.css'],
  directives: [StatisticsComponent]
})

export class DashboardComponent implements OnInit, AfterViewInit {

  channelName: {channelName: string; token: string};

  constructor(private authService: AuthService) {}

  ngOnInit():any {
    this.channelName = this.authService.getShikashiCredentials().channelName;
  }

  ngAfterViewInit():any {
    new Chat().start(this.channelName);
  }

}
