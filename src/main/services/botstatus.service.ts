 import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/Rx';
import {HttpService} from '../../shared/services/http.service';
import {AuthService} from '../../shared/services/auth.service';

@Injectable()

/**
 * This service uses the HttpService to communicate to the server and modify the bot status
 */
export class BotstatusService {

  channelsEndpoint = 'channels/';

  constructor (private httpService: HttpService, private authService: AuthService) {}

  /**
   * Gets the status of the bot.
   * @return getStatusEvent This event is fired as soon as a response is received. Is either true/false (indicating
   * if the bot is active/inactive) or null (in case of error).
   */
  getStatus() {
    var getStatusEvent = new EventEmitter();

    this.httpService.get(this.channelsEndpoint + '[CHANNELNAME]')
      .map(response => response.json())
      .subscribe(
        data => {
          console.log('Successfully received bot status.');
          getStatusEvent.emit(data.visiting);
        },
        error => {
          console.log('Could not load bot status.');
          getStatusEvent.emit(null);
        }
      );

    return getStatusEvent;
  }

  /**
   * Makes the bot join the channel (set bot active)
   * @return joinChannelEvent This event is fired as soon as a response is received. Is either true (success)
   * or false (in case of error).
   */
  joinChannel() {
    var joinChannelEvent = new EventEmitter();

    const credentials = this.authService.getShikashiCredentials();
    var channel = {channelName: credentials.channelName};

      this.httpService.post(this.channelsEndpoint, JSON.stringify(channel))
      .subscribe(
        data => {
          console.log('Bot joined channel successfully.');
          joinChannelEvent.emit(true);
        },
        error => {
          console.log('Bot could not join the channel.');
          joinChannelEvent.emit(false);
        }
      );

    return joinChannelEvent;
  }

  /**
   * Makes the bot leave the channel (set bot inactive)
   * @return leaveChannelEvent This event is fired as soon as a response is received. Is either true (success)
   * or false (in case of error).
   */
  leaveChannel() {
    var leaveChannelEvent = new EventEmitter();

    this.httpService.delete(this.channelsEndpoint + '[CHANNELNAME]')
      .subscribe(
        data => {
          console.log('Bot left channel successfully.');
          leaveChannelEvent.emit(true);
        },
        error => {
          console.log('Bot could not leave the channel.');
          leaveChannelEvent.emit(false);
        }
      );

    return leaveChannelEvent;
  }



}
