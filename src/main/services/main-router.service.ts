import {Injectable} from '@angular/core';
import {Router} from '@angular/router-deprecated';

@Injectable()

/**
 * This service holds a reference to the mainRouterOutlet.
 * The reference is set by the mainRouterOutlet itself and used by the navbarComponent.
 */
export class MainRouterService {

  private router: Router;

  setRouter(router: Router) {
    this.router = router;
  }

  getRouter() {
    return this.router;
  }

}
