import {Component} from '@angular/core';
import {RouteConfig} from '@angular/router-deprecated';
import {NavbarComponent} from './navbar.component';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {DashboardComponent} from '../../dashboard/components/dashboard.component';
import {AnnouncementsComponent} from '../../announcements/components/announcements.component';
import {CommandsComponent} from '../../commands/components/commands.component';
import {FiltersComponent} from '../../filters/components/filters.component';
import {RegularsComponent} from '../../regulars/components/regulars.component';
import {MainRouterOutlet} from '../directives/main-router-outlet.directive';
import {MainRouterService} from '../services/main-router.service';
import {TrendingComponent} from "../../trending/components/trending.component";

@Component({
  selector: 'sb-main',
  moduleId: module.id,
  templateUrl: './main.component.html',
  directives: [MainRouterOutlet, NavbarComponent, ToolbarComponent],
  providers: [MainRouterService]
})

@RouteConfig([
  { path: '/dashboard', name: 'Dashboard', component: DashboardComponent, useAsDefault: true },
  { path: '/announcements', name: 'Announcements', component: AnnouncementsComponent },
  { path: '/commands', name: 'Commands', component: CommandsComponent },
  { path: '/filters', name: 'Filters', component: FiltersComponent },
  { path: '/regulars', name: 'Regulars', component: RegularsComponent },
  { path: '/trending', name: 'Trending', component: TrendingComponent }
])

/**
 * This component includes the toolbar, the navbar and the main-router-outlet.
 */
export class MainComponent {}
