import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, Router} from '@angular/router-deprecated';
import {MainRouterService} from '../services/main-router.service';

@Component({
  selector: 'sb-navbar',
  moduleId: module.id,
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  directives: [ROUTER_DIRECTIVES]
})

/**
 * This component includes buttons to navigate through the main-router-outlet.
 */
export class NavbarComponent {
  router: Router;

  constructor(private mainRouterService: MainRouterService) {}

  /**
   * Checks if a route is active
   * @param routerLink The name of the route to check
   * @return true/false
   */
  isRouteActive(routerLink: string) {
    this.router = this.mainRouterService.getRouter();
    if(this.router) {
      return this.router.isRouteActive(this.router.generate([routerLink]));
    }
    return false;
  }
}
