import {Component, OnInit} from '@angular/core';
import {BotstatusService} from '../../services/botstatus.service';
import {AlertService} from '../../../app/services/alert.service';

@Component({
  selector: 'sb-botstatus',
  moduleId: module.id,
  templateUrl: './botstatus.component.html',
  styleUrls: ['./botstatus.component.css'],
  providers: [BotstatusService]
})

/**
 * This component includes a toggle to activate/deactivate the bot (moderate the channel)
 */
export class BotstatusComponent implements OnInit {

  isVisiting: boolean;

  constructor(private botstatusService: BotstatusService, private alertService: AlertService) {}

  ngOnInit():any {
    this.botstatusService.getStatus()
      .subscribe((data:boolean) => { if (data) {
          this.isVisiting = data;
        } });
  }

  /**
   * Uses the botstatusService to either activate or deactivate the bot.
   */
  setStatus(checkbox:any) {
    if(checkbox.checked) {
      this.botstatusService.joinChannel()
        .subscribe((success:boolean) => {
          if(success) {
            this.isVisiting = true;
            checkbox.checked = true;
          } else {
            this.isVisiting = false;
            checkbox.checked = false;
            this.alertService.alert({type: 'alert-danger', message: 'Bot could not join your channel.', autoResolve: true});
          }
        });
    } else {
      this.botstatusService.leaveChannel()
        .subscribe((success:boolean) => {
          if(success) {
            this.isVisiting = false;
            checkbox.checked = false;
          } else {
            this.isVisiting = true;
            checkbox.checked = true;
            this.alertService.alert({type: 'alert-danger', message: 'Bot could not leave your channel.', autoResolve: true});
          }
        });
    }
  }
}
