import {Component} from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service';
import {Router} from '@angular/router-deprecated';
import {BotstatusComponent} from './botstatus.component';

@Component({
  selector: 'sb-toolbar',
  moduleId: module.id,
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
  directives: [BotstatusComponent]
})

/**
 * This component includes a the botstatus component and a log-out button
 */
export class ToolbarComponent {

  constructor(private router: Router, private authService: AuthService) {}

  /**
   * Uses the authService to invalidate the shikashi-token and redirects the user
   * to the landing page.
   */
  logOut() {
    this.authService.invalidateShikashiToken()
      .subscribe(() => this.router.parent.navigate(['Landing']));
  }
}
