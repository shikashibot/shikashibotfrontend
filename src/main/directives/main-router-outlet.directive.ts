import {RouterOutlet, Router, ComponentInstruction} from '@angular/router-deprecated';
import {Directive, DynamicComponentLoader, Input, OnInit, ViewContainerRef} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {MainRouterService} from '../services/main-router.service';
import {AlertService} from '../../app/services/alert.service';

@Directive({
  selector: 'main-router-outlet'
})

/**
 * This directive is responsible for the routing between the main pages.
 * If the user is not logged in it redirects automatically to the landing page.
 */
export class MainRouterOutlet extends RouterOutlet implements OnInit {

  constructor(viewContainerRef: ViewContainerRef, dynamicComponentLoader: DynamicComponentLoader, private router: Router, @Input() nameAttr: string, private authService: AuthService, private mainRouterService: MainRouterService, private alertService: AlertService) {
    super(viewContainerRef, dynamicComponentLoader, router, nameAttr);
  }

  ngOnInit() {
    this.mainRouterService.setRouter(this.router);
  }

  activate(nextInstruction: ComponentInstruction): Promise<any> {
    if (!this.authService.getShikashiCredentials()) {
      this.router.parent.navigate(['Landing']);
      this.alertService.alert({type: 'alert-danger', message: 'You have been logged out', autoResolve: true});
    }

    return super.activate(nextInstruction);
  }
}
