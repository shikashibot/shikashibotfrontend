/**
 * Caches all files which match a file type in the cachableFileTypes array
 * Responds to requests straight away with the cached version if a newer version is available via a network request the client gets notified
 */

const version = '1';

var wasUpdated = false;

self.addEventListener('activate', function(event) {
  caches.delete('shikashi');
});

self.addEventListener('fetch', function(event) {
  var filename = event.request.url.split('/').pop();
  if (filename.indexOf('?') > -1) { filename = filename.substring(0, filename.lastIndexOf('?')) }

  if (filename === 'app.js') {
    event.respondWith(
      caches.open('shikashi').then(function(cache) {
        return cache.match(event.request).then(function (response) {
          if (response) { // Return cached version and fetch resource from server in background
            fetchInBackground(event.request);
            return response;
          } else { // Fetch Resource from server, save resource to cache and return the resource
            return fetch(event.request).then(function (response) {
              cache.put(event.request, response.clone());
              return response;
            });
          }

        });
      })
    );
  }

  function fetchInBackground(request) {
    caches.open('shikashi').then(function(cache) {
      fetch(event.request).then(function (response) {
        var networkResponse = response;
        cache.match(event.request).then(function (cacheResponse) {
          if(networkResponse.headers.get('last-modified') > cacheResponse.headers.get('last-modified')) {
            cache.put(event.request, networkResponse.clone());
            wasUpdated = true;
          }
        });
      });
    });
  }
});


self.addEventListener('message', function(event) {
  if (event.data.command === 'wasUpdated') {
    event.ports[0].postMessage({ wasUpdated: wasUpdated });
    wasUpdated = false;
  }
});
