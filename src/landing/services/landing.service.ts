import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/Rx';

@Injectable()

/**
 * This service enables app-router-outlet and the landing.component to communicate and share
 * the current logging in state.
 */
export class LandingService {
  private isLoggingIn: boolean;
  private isLoggingInEventEmitter: EventEmitter<any> = new EventEmitter();

  /**
   * Returns the issLoggingInEventEmitter the landing.component listens to.
   * @return isLoggingInEventEmitter
   */
  isLoggingInEvent() {
    // Emit the event immediately so the subscriber gets the current value
    // (isLoggingIn is set in app-router, before the landing component exists)
    setTimeout(() => this.isLoggingInEventEmitter.emit(this.isLoggingIn), 0);
    return this.isLoggingInEventEmitter;
  }

  /**
   * Emits the isLoggingInEventEmitter event.
   * @param loggingIn A bool indicating if the logging-in process is currently active.
   * @return isLoggingInEventEmitter The event is emitted and carries the current state.
   */
  setIsLoggingIn(loggingIn: boolean) {
    this.isLoggingIn = loggingIn;
    this.isLoggingInEventEmitter.emit(loggingIn);
  }
}
