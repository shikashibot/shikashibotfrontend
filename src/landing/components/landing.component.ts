import {Component, OnInit} from '@angular/core';
import {LandingService} from '../services/landing.service';

@Component({
  selector: 'sb-landing',
  moduleId: module.id,
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})

/**
 * This component includes the twitch-sign-in button to redirect the user to twitch for oAuth.
 * It listens to landingService.isLoggingInEvent to show a logging in animation
 */
export class LandingComponent implements OnInit {

  twitchOAuthUrl = 'https://api.twitch.tv/kraken/oauth2/authorize?response_type=code&client_id=<%= shikashiConfig.TWITCH_API_CLIENT_ID %>&redirect_uri=<%= shikashiConfig.TWITCH_API_REDIRECT_URI %>&scope=user_read';
  isLoggingIn = false;

  constructor (private landingService: LandingService) {}

  ngOnInit():any {
    this.landingService.isLoggingInEvent()
      .subscribe((isLoggingIn:boolean) => {
        this.isLoggingIn = isLoggingIn;
      }
    );
  }

}
