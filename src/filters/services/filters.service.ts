import {EventEmitter, Injectable} from '@angular/core';
import {HttpService} from '../../shared/services/http.service';

@Injectable()

/**
 * This service uses the HttpService to communicate to the server and retrieve the filters
 */
export class FiltersService {

  filtersEndpoint = 'channels/[CHANNELNAME]/filters';

  constructor(private httpService: HttpService) {}

  /**
   * Gets all filters.
   * @return getFiltersEvent This event is fired as soon as a response is received. Carries either the list of filters
   * or null (in case of failure)
   */
  getFilters() {
    var getFiltersEvent = new EventEmitter();

    this.httpService.get(this.filtersEndpoint)
      .map(response => response.json())
      .subscribe(
        data => {
          console.log('Successfully received filters data');
          getFiltersEvent.emit(data);
        },
        error => {
          console.log('Could not load filters data');
          getFiltersEvent.emit(null);
        }
      );

    return getFiltersEvent;
  }

}
