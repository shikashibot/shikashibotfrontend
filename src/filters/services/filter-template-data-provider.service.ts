import {Injectable} from '@angular/core';

@Injectable()


/**
 * This service holds the template data (for the html) for each filterType.
 */
export class FilterTemplateDataProviderService {

  capsFilterData = {filterTypeLabel: 'Maximum capital letters',
    filterTypeTooltip: 'The maximum number of capital letters in one message.'};
  emoteFilterData = {filterTypeLabel: 'Maximum emoticons',
    filterTypeTooltip: 'The maximum number of emoticons in one message.'};
  lengthFilterData = {filterTypeLabel: 'Maximum message length',
    filterTypeTooltip: 'The maximum number of characters in one message.'};
  repeatFilterData = {filterTypeLabel: 'Maximum repetition',
    filterTypeTooltip: 'The maximum number of the appearance of one and the same word in one message.'};
  symbolFilterData = {filterTypeLabel: 'Maximum symbols',
    filterTypeTooltip: 'The maximum number of special characters in one message.'};
  domainFilterData = {filterTypeLabel: 'Domain filter',
    filterTypeTooltip: 'Filters all messages which include domains. You can allow certain domains in the whitelist.'};
  wordFilterData = {filterTypeLabel: 'Word filter',
    filterTypeTooltip: 'A blacklist of words which are not allowed your chat.'};

  /**
   * Serves the template data.
   * @param filterId
   * @return filterService The appropriate template data for the passed filterId.
   */
  getData(filterId: string) {
    switch (filterId) {
      case 'Caps':
        return this.capsFilterData;
      case 'Emote':
        return this.emoteFilterData;
      case 'Length':
        return this.lengthFilterData;
      case 'Repeat':
        return this.repeatFilterData;
      case 'Symbol':
        return this.symbolFilterData;
      case 'Domain':
        return this.domainFilterData;
      case 'Word':
        return this.wordFilterData;
    }
    return null;
  }

}
