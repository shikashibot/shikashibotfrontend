import {Injectable} from '@angular/core';
import {SimpleFilterService} from './simple-filter.service';
import {HttpService} from '../../shared/services/http.service';

@Injectable()

/**
 * This service holds a instance of SimpleFilterService for each
 * filterType.
 */
export class FilterServiceProviderService {

  capsFilterService = new SimpleFilterService(this.httpService, 'caps');
  emoteFilterService = new SimpleFilterService(this.httpService, 'emote');
  lengthFilterService = new SimpleFilterService(this.httpService, 'length');
  repeatFilterService = new SimpleFilterService(this.httpService, 'repeat');
  symbolFilterService = new SimpleFilterService(this.httpService, 'symbol');
  domainFilterService = new SimpleFilterService(this.httpService, 'domain');
  wordFilterService = new SimpleFilterService(this.httpService, 'word');

  constructor(public httpService: HttpService) {}

  /**
   * Serves SimpleFilterService instances.
   * @param filterId
   * @return filterService The appropriate filterService for the passed filterId.
   */
  getFilter(filterId: string) {
    switch (filterId) {
      case 'Caps':
            return this.capsFilterService;
      case 'Emote':
            return this.emoteFilterService;
      case 'Length':
            return this.lengthFilterService;
      case 'Repeat':
            return this.repeatFilterService;
      case 'Symbol':
        return this.symbolFilterService;
      case 'Domain':
        return this.domainFilterService;
      case 'Word':
        return this.wordFilterService;
    }
    return null;
  }

}
