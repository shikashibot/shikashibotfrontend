import {EventEmitter, Injectable} from '@angular/core';
import {HttpService} from '../../shared/services/http.service';

@Injectable()

/**
 * This service uses the HttpService to communicate to the server and changes filters.
 * It is used for all types of filters. The passed filterId determines the REST-endpoint.
 */
export class SimpleFilterService {

  filtersEndpoint = 'channels/[CHANNELNAME]/filters/';

  constructor(private httpService: HttpService, private filterId: string) {}

  /**
   * Changes a filter for a channel.
   * @param newFilter A object holding the data for the new filter: { limit: number, outputMessage: string, timeoutSeconds: number, removeType: number }
   * @return changeFilterEvent This event is fired as soon as a response is received. Is either true (in case of success) or
   * false (in case of failure)
   */
  changeFilter(newFilter:{ limit: number, outputMessage: string, timeoutSeconds: number, removeType: number }) {
    var changeFilterEvent = new EventEmitter();

    this.httpService.put(this.filtersEndpoint + this.filterId, JSON.stringify(newFilter))
      .subscribe(
        data => {
          console.log('Successfully changed filter (' + this.filterId + ')');
          changeFilterEvent.emit(true);
        },
        error => {
          console.log('Could not change filter (' + this.filterId + ')');
          changeFilterEvent.emit(false);
        }
      );

    return changeFilterEvent;
  }


}
