import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {SimpleFilterService} from '../services/simple-filter.service';
import {FilterServiceProviderService} from '../services/filter-service-provider.service';
import {FilterFormComponent} from './filter-form.component';
import {AlertService} from '../../app/services/alert.service';

@Component({
  selector: 'sb-filter-panel',
  moduleId: module.id,
  templateUrl: './filter-panel.component.html',
  styleUrls: ['./filter-panel.component.css'],
  directives: [FilterFormComponent]
})

/**
 * This component includes a expandable div container. A toggle to activate /
 * deactivate the filter is shown. It uses the filterServiceProvider to get a reference
 * to the appropriate filterService depending on the filter type.
 * @param filter The filter object as received from the server or null.
 * @return filterChanged This event is emitted if the announcements have changed in any way (a announcement was created/deleted/changed)
 */
export class FilterPanelComponent implements OnInit {
  @Input() filter: any;

  @Output() filtersChanged = new EventEmitter<any>();

  filterService: SimpleFilterService;

  constructor(private filterServiceProvider: FilterServiceProviderService, private alertService: AlertService) {}

  ngOnInit():any {
    this.filterService = this.filterServiceProvider.getFilter(this.filter.identifier);
  }

  /**
   * Sets the enabled property equal to the user input and passes the filter
   * object to the filterService to change the filter-state on the server
   * @param checkbox A object representing the checkbox ui element
   */
  setIsActive(checkbox:any) {
    this.filter.enabled = checkbox.checked;

    this.filterService.changeFilter(this.filter)
      .subscribe((success:boolean) => {
        if (!success) {
          this.filter.enabled ? this.filter.enabled = false : this.filter.enabled = true;
          this.alertService.alert({type: 'alert-danger', message: 'Filter state could not be changed.', autoResolve: true});
        }
      });
  }

  /**
   * Emits the filtersChanged event.
   * @return filtersChanged This event indicates that the filters have changed in any way.
   */
  onAnnouncementsChanged() {
    this.filtersChanged.emit(null);
  }
}
