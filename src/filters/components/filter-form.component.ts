import {Component, AfterViewInit, OnInit, Input, Output} from '@angular/core';
import {ControlGroup, Validators, FormBuilder, Control} from '@angular/common';
import {FilterServiceProviderService} from '../services/filter-service-provider.service';
import {SimpleFilterService} from '../services/simple-filter.service';
import {FilterTemplateDataProviderService} from '../services/filter-template-data-provider.service';
import {FormListComponent} from './../../shared/components/form-list.component';
import {AlertService} from '../../app/services/alert.service';
import {EventEmitter} from '@angular/core';
declare var jQuery: any;

@Component({
  selector: 'sb-filter-form',
  moduleId: module.id,
  templateUrl: './filter-form.component.html',
  styleUrls: ['./filter-form.component.css'],
  directives: [FormListComponent]
})

/**
 * This component includes the input fields for the remove type, timeout seconds
 * and the output message. It can either show a input field for a limit-value or a
 * editable list (depending on the filter-type). It includes also a "change filter"
 * button. It uses the filterServiceProvider and filterTemplateDataProvider to get a
 * reference to the appropriate filterService and the appropriate template data (for
 * the html) depending on the filter type.
 * @param filter The filter object as received from the server
 * @return filtersChanged This event is emitted if a changeCommand action failed (UI and data are out of sync)
 */
export class FilterFormComponent implements OnInit, AfterViewInit {
  @Input() filter: any;

  @Output() filtersChanged = new EventEmitter<any>();

  filterType: string;
  form: ControlGroup;
  templateData: any;
  submitted = false;

  filterService: SimpleFilterService;

  constructor(private formBuilder: FormBuilder, private filterServiceProviderService: FilterServiceProviderService, private filterTemplateDataProviderService: FilterTemplateDataProviderService, private alertService: AlertService) {}

  ngOnInit():any {
    this.filterType = this.filter.identifier === 'Domain' || this.filter.identifier === 'Word' ? 'list' : 'limit';

    this.form = this.formBuilder.group({
      'removeType': [this.filter ? String(this.filter.removeType) : String(1), Validators.required],
      'timeoutSeconds': [this.filter ? this.filter.timeoutSeconds : 0],
      'outputMessage': [this.filter ? this.filter.outputMessage : '', Validators.required]
    }, {validator: this.timeoutSecondsValid()});
    if (this.filterType === 'limit') {
      this.form.addControl('limit', new Control(this.filter ? this.filter.limit : 10, Validators.compose([this.limitValid])));
    }

    this.templateData = this.filterTemplateDataProviderService.getData(this.filter.identifier);

    this.filterService = this.filterServiceProviderService.getFilter(this.filter.identifier);
  }

  ngAfterViewInit() {
    // Activate Bootstrap tooltips
    jQuery('.'+this.filter.identifier+'-tooltip').tooltip();
  }


  /**
   * Passes a new filter object (using the data from the input fields) to the
   * filterService to change the filter on the server.
   * @return filtersChanged This event is emitted, if the action was unsuccessful (UI and data are out of sync)
   */
  changeFilter() {
    this.submitted = true;

    if(this.form.valid) {

      var newFilter = this.form.value;
      // Set timeoutSeconds in cas of NaN. (The form allows NaN but the server expects a value)
      if (isNaN(newFilter.timeoutSeconds)) {newFilter.timeoutSeconds = 0;}

      this.filterService.changeFilter(newFilter)
        .subscribe((success:boolean) => {
          if (success) {
            this.alertService.alert({type: 'alert-success', message: 'Changes saved.', autoResolve: true});
          } else {
            this.filtersChanged.emit(null);
            this.alertService.alert({type: 'alert-danger', message: 'Changes could not be saved.', autoResolve: true});
          }
        });
    }
  }

  /**
   * Checks if the user-input for the timeoutSeconds is valid. Since the validation is based on multiple input fields the validation
   * belongs to the whole form rather than to the input field timeoutSeconds.
   * @return errorObject A object to indicate that the validation was unsuccessful (returns nothing if the validation was successful)
   */
  timeoutSecondsValid() {
    return (group: ControlGroup): {[key: string]: any} => {
      let removeType = group.controls['removeType'];
      let timeoutSeconds = group.controls['timeoutSeconds'];

      if (removeType.value === '1') {
        if (isNaN(timeoutSeconds.value)) {
          return {timeoutSecondsIsNaN: true};
        } else if (timeoutSeconds.value < 0) {
          return {timeoutSecondsIsNegative: true};
        }
      }
      return null;
    };
  }

  /**
   * Checks if the user-input for the limit is valid.
   * @param control A angular2 control object representing the input field
   * @return errorObject A object to indicate that the validation was unsuccessful (returns nothing if the validation was successful)
   */
  limitValid(control: Control): any {
    if (isNaN(control.value)) {
      return {NaN: true};
    } else if (control.value < 0) {
      return {negative: true};
    }
  }

}
