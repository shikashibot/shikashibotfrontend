import {Component, OnInit} from '@angular/core';
import {FilterPanelComponent} from './filter-panel.component';
import {FiltersService} from '../services/filters.service';
import {FilterServiceProviderService} from '../services/filter-service-provider.service';
import {FilterTemplateDataProviderService} from '../services/filter-template-data-provider.service';
import {AlertService} from '../../app/services/alert.service';

@Component({
  selector: 'sb-filters',
  moduleId: module.id,
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css'],
  directives: [FilterPanelComponent],
  providers: [FiltersService, FilterServiceProviderService, FilterTemplateDataProviderService]
})

/**
 * This component includes a list of filter-panels. It fetches a list of all filters from the server.
 * If the filtersChanged event is fired it reloads the filters.
 */
export class FiltersComponent implements OnInit {

  private loadingComplete: boolean;
  private filters: any;

  constructor(private filtersService: FiltersService, private alertService: AlertService) {}

  ngOnInit():any {
    this.loadFilter();
  }

  /**
   * Uses the filtersService to load all filters from the server.
   */
  loadFilter() {
    this.filtersService.getFilters()
      .subscribe((data:any) => {
        if (data) {
          this.filters = data;
          this.loadingComplete = true;
        } else {
          this.alertService.alert({type: 'alert-danger', message: 'Could not load filters.', autoResolve: true});
        }
      });
  }

}
