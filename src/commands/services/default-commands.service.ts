import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/Rx';
import {HttpService} from '../../shared/services/http.service';

@Injectable()

/**
 * This service uses the HttpService to communicate to the server and modify the default commands of a channel
 */
export class DefaultCommandService {

  defaultCommandsEndpoint = 'channels/[CHANNELNAME]/commands';

  constructor (private httpService: HttpService) {}

  /**
   * Gets all default commands of a channel.
   * @return getCommandsEvent This event is fired as soon as a response is received. Carries either the list of commands
   * or null (in case of failure)
   */
  getCommands() {
    var getCommandsEvent = new EventEmitter();

    this.httpService.get(this.defaultCommandsEndpoint)
      .map(response => response.json())
      .subscribe(
        data => {
          console.log('Successfully received default commands.');
          getCommandsEvent.emit(data);
        },
        error => {
          console.log('Could not load default commands.');
          getCommandsEvent.emit(null);
        }
      );

    return getCommandsEvent;
  }

  /**
   * Changes a default command for a channel.
   * @param newCommand A object holding the data for the new command: { commandId: string, prefix: string, minRank: number, enabled: boolean }
   * @return changeCommandEvent This event is fired as soon as a response is received. Is either true (in case of success) or
   * false (in case of failure)
   */
  changeCommand(newCommand:{ commandId: string, prefix: string, minRank: number, enabled: boolean }) {
    var changeCommandEvent = new EventEmitter();

    this.httpService.put(this.defaultCommandsEndpoint + '/' + newCommand.commandId, JSON.stringify(newCommand))
      .subscribe(
        data => {
          console.log('Successfully changed the isActive state.');
          changeCommandEvent.emit(true);
        },
        error => {
          console.log('Could not change the isActive state.');
          changeCommandEvent.emit(false);
        }
      );

    return changeCommandEvent;
  }

}
