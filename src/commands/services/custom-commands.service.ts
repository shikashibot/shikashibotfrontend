import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/Rx';
import {HttpService} from '../../shared/services/http.service';

@Injectable()

/**
 * This service uses the HttpService to communicate to the server and modify the custom commands of a channel
 */
export class CustomCommandService {

  customCommandsEndpoint = 'channels/[CHANNELNAME]/customCommands'

  constructor (private httpService: HttpService) {}

  /**
   * Gets all custom commands of a channel.
   * @return getCommandsEvent This event is fired as soon as a response is received. Carries either the list of commands
   * or null (in case of failure)
   */
  getCommands() {
    var getCommandsEvent = new EventEmitter();

    this.httpService.get(this.customCommandsEndpoint)
      .map(response => response.json())
      .subscribe(
        data => {
          console.log('Successfully received custom commands.');
          getCommandsEvent.emit(data);
        },
        error => {
          console.log('Could not load custom commands.');
          getCommandsEvent.emit(null);
        }
      );

    return getCommandsEvent;
  }

  /**
   * Adds an custom command for a channel.
   * @param newCommand A object holding the data for the new command: { output: string, commandId: string, minRank: number , isActive: boolean }
   * @return addCommandEvent This event is fired as soon as a response is received. Is either true (in case of success) or
   * false (in case of failure).
   */
  addCommand(newCommand:{ output: string, commandId: string, minRank: number, isActive: boolean }) {
    var addCommandEvent = new EventEmitter();

    this.httpService.post(this.customCommandsEndpoint + '/' + newCommand.commandId, JSON.stringify(newCommand))
      .subscribe(
        data => {
          console.log('Successfully added new command.');
          addCommandEvent.emit(true);
        },
        error => {
          console.log('Could not add new command.');
          addCommandEvent.emit(false);
        }
      );

    return addCommandEvent;
  }

  /**
   * Deletes an custom command for a channel.
   * @param commandId The id of the command.
   * @return deleteCommandEvent This event is fired as soon as a response is received. Is either true (in case of success) or
   * false (in case of failure).
   */
  deleteCommand(commandId: string) {
    var deleteCommandEvent = new EventEmitter();

    this.httpService.delete(this.customCommandsEndpoint + '/' + commandId)
      .subscribe(
        data => {
          console.log('Successfully deleted command.');
          deleteCommandEvent.emit(true);
        },
        error => {
          console.log('Could not delete command.');
          deleteCommandEvent.emit(false);
        }
      );

    return deleteCommandEvent;
  }

  /**
   * Changes a announcement for a channel.
   * @param newCommand A object holding the data for the new command: { output: string, commandId: string, minRank: number , isActive: boolean }
   * @return changeAnnouncementEvent This event is fired as soon as a response is received. Is either true (in case of success) or
   * false (in case of failure).
   */
  setIsActive(newCommand:{ output: string, commandId: string, minRank: number , isActive: boolean }) {
    var setIsActiveEvent = new EventEmitter();

    this.httpService.put(this.customCommandsEndpoint + '/' + newCommand.commandId, JSON.stringify(newCommand))
      .subscribe(
        data => {
          console.log('Successfully changed the isActive state.');
          setIsActiveEvent.emit(true);
        },
        error => {
          console.log('Could not change the isActive state.');
          setIsActiveEvent.emit(false);
        }
      );

    return setIsActiveEvent;
  }

}
