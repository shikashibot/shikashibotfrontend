import {Component, OnInit} from '@angular/core';
import {DefaultCommandService} from '../../services/default-commands.service';
import {DefaultCommandPanelComponent} from './default-command-panel.component';
import {AlertService} from '../../../app/services/alert.service';

@Component({
  selector: 'sb-default-commands',
  moduleId: module.id,
  templateUrl: './default-commands.component.html',
  styleUrls: ['./default-commands.component.css'],
  providers: [DefaultCommandService],
  directives: [DefaultCommandPanelComponent]
})

/**
 * This component includes a list of default-command-panels. It fetches a list of all default
 * commands from the server. If the commandsChanged event is fired it reloads the commands.
 */
export class DefaultCommandsComponent implements OnInit {

  private loadingComplete: boolean;
  private defaultCommands: any;

  constructor(private defaultCommandService: DefaultCommandService, private alertService: AlertService) {}

  ngOnInit() {
    this.loadCommands();
  }

  /**
   * Uses the defaultCommandService to load all commands from the server.
   */
  loadCommands() {
    this.defaultCommandService.getCommands()
      .subscribe((data:any) => {
        if (data) {
          this.defaultCommands = data;
          this.loadingComplete = true;
        } else {
          this.alertService.alert({type: 'alert-danger', message: 'Could not load default commands.', autoResolve: true});
        }
      });
  }

}
