import {Component, Input, OnInit, Output, AfterViewInit} from '@angular/core';
import {FormBuilder, ControlGroup, Validators} from '@angular/common';
import {EventEmitter} from '@angular/core';
import {AlertService} from '../../../app/services/alert.service';
import {DefaultCommandService} from '../../services/default-commands.service';
declare var jQuery: any;

@Component({
  selector: 'sb-default-command-form',
  moduleId: module.id,
  templateUrl: './default-command-form.component.html',
  styleUrls: ['./default-command-form.component.css']
})

/**
 * This component includes the input fields for the command id
 * and the minimum rank as well as a "change command" button.
 * @param command The command object as received from the server or null.
 * @param index The index of the command in the command array received from the server or 'new-command'.
 * @return commandsChanged This event is emitted if the commands have changed in any way.
 */
export class DefaultCommandFormComponent implements OnInit, AfterViewInit {
  @Input() command: any;
  @Input() index: any;

  @Output() commandsChanged = new EventEmitter<any>();

  form: ControlGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private defaultCommandService: DefaultCommandService, private alertService: AlertService) {}

  ngOnInit():any {
    this.form = this.formBuilder.group({
      'prefix': [this.command.prefix, Validators.compose([
        Validators.required,
        Validators.pattern('[a-zA-Z0-9]*')
      ])],
      'minRank': [this.command.minRank, Validators.required]
    });
  }

  ngAfterViewInit() {
    // Activate Bootstrap tooltips
    jQuery('[data-toggle="tooltip"]').tooltip();
  }

  /**
   * Passes a new command object (using the data from the input fields) to the
   * defaultCommandService to change the command on the server.
   * @return commandsChanged This event is emitted, in success and error case, as soon as the action is complete.
   */
  changeCommand() {
    this.submitted = true;
    if (this.form.valid) {

      var newCommand = this.form.value;
      newCommand.commandId = this.command.commandId;
      newCommand.enabled = this.command.enabled;

      this.defaultCommandService.changeCommand(newCommand)
        .subscribe((success:boolean) => {
          if (success) {
            this.commandsChanged.emit(null);
            this.alertService.alert({type: 'alert-success', message: 'Command changed.', autoResolve: true});
          } else {
            this.commandsChanged.emit(null);
            this.alertService.alert({type: 'alert-danger', message: 'Command could not be changed.', autoResolve: true});
          }
        });

    }
  }

}
