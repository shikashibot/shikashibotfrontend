import {Component, Input, EventEmitter, Output} from '@angular/core';
import {DefaultCommandFormComponent} from './default-command-form.component';
import {AlertService} from '../../../app/services/alert.service';
import {DefaultCommandService} from '../../services/default-commands.service';

@Component({
  selector: 'sb-default-command-panel',
  moduleId: module.id,
  templateUrl: './default-command-panel.component.html',
  styleUrls: ['./default-command-panel.component.css'],
  providers: [DefaultCommandService],
  directives: [DefaultCommandFormComponent]
})

/**
 * This component includes a expandable div container. A toggle to activate /
 * deactivate the command is shown.
 * @param command The command object as received from the server.
 * @param index The index of the command in the command array received from the server.
 * @return commandsChanged This event is emitted if the commands have changed in any way.
 */
export class DefaultCommandPanelComponent {
  @Input() command: any;
  @Input() index: any;

  @Output() commandsChanged = new EventEmitter<any>();

  constructor(private defaultCommandService: DefaultCommandService, private alertService: AlertService) {}

  /**
   * Sets the isActive property equal to the user input and passes the command
   * object to the defaultCommandsService to change the command-state on the server
   * @param checkbox A object representing the checkbox ui element
   */
  setIsActive(checkbox:any) {
    this.command.enabled = checkbox.checked;

    this.defaultCommandService.changeCommand(this.command)
      .subscribe((success:boolean) => {
        if (!success) {
          this.command.isActive ? this.command.isActive = false : this.command.isActive = true;
          this.alertService.alert({type: 'alert-danger', message: 'Command state could not be changed.', autoResolve: true});
        }
      });
  }

  /**
   * Emits the commandsChanged event.
   * @return commandsChanged This event indicates that the commands have changed in any way.
   */
  onCommandsChanged(input:any) {
    this.commandsChanged.emit(null);
  }
}
