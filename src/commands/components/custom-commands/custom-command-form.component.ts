import {Component, Input, OnInit, Output, AfterViewInit} from '@angular/core';
import {FormBuilder, ControlGroup, Validators, Control} from '@angular/common';
import {CustomCommandService} from '../../services/custom-commands.service';
import {EventEmitter} from '@angular/core';
import {AlertService} from '../../../app/services/alert.service';
declare var jQuery: any;

@Component({
  selector: 'sb-custom-command-form',
  moduleId: module.id,
  templateUrl: './custom-command-form.component.html',
  styleUrls: ['./custom-command-form.component.css']
})

/**
 * This component includes the input fields for the command id, message
 * and the minimum rank. It can either show a "change command" and a
 * "delete command" button (for existing commands) or a "create
 * command" button (for creating a new command)
 * @param command The command object as received from the server or null.
 * @param index The index of the command in the command array received from the server or 'new-command'
 * @return commandsChanged This event is emitted if the commands have changed in any way (a command was created/deleted/changed)
 */
export class CustomCommandFormComponent implements OnInit, AfterViewInit {
  @Input() command: any;
  @Input() index: any;

  @Output() commandsChanged = new EventEmitter<any>();

  form: ControlGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, private customCommandService: CustomCommandService, private alertService: AlertService) {}

  ngOnInit():any {
    this.form = this.formBuilder.group({
      'commandId': [this.command ? this.command.commandId : '', Validators.compose([
        Validators.required,
        Validators.pattern('[a-zA-Z0-9]*')
      ])],
      'output': [this.command ? this.command.output : '', Validators.required],
      'minRank': [this.command ? this.command.minRank : 0, Validators.required]
    });
  }

  ngAfterViewInit() {
    // Activate Bootstrap tooltips
    jQuery('[data-toggle="tooltip"]').tooltip();
  }

  /**
   * Passes the data from the input fields to the customCommandsService to create
   * a command on the server
   * @return commandsChanged This event is emitted if command was successfully added
   */
  addCommand() {
    this.submitted = true;
    if(this.form.valid) {

      var newCommand = this.form.value;
      newCommand.isActive = true;

      this.customCommandService.addCommand(newCommand)
        .subscribe((success:boolean) => {
          if(success) {
            this.commandsChanged.emit(null);
            this.alertService.alert({type: 'alert-success', message: 'Command added.', autoResolve: true});

            this.submitted = false;
            (<Control> this.form.controls['commandId']).updateValue('', true);
            (<Control> this.form.controls['output']).updateValue('', true);
            (<Control> this.form.controls['minRank']).updateValue('', true);
          } else {
            this.alertService.alert({type: 'alert-danger', message: 'Command could not be added.', autoResolve: true});
          }
        });
    }
  }

  /**
   * Passes the commandId to the customCommandsService to delete the command
   * on the server
   * @return commandsChanged This event is emitted if the command was successfully deleted
   */
  deleteCommand() {
    this.customCommandService.deleteCommand(this.command.commandId)
      .subscribe((success:boolean) => {
        if(success) {
          this.commandsChanged.emit(null);
          this.alertService.alert({type: 'alert-success', message: 'Command deleted.', autoResolve: true});
        } else {
          this.alertService.alert({type: 'alert-danger', message: 'Command could not be deleted.', autoResolve: true});
        }
      });
  }

  /**
   * Passes the commandId to the customCommandsService to delete the command on the server.
   * Then passes new command object (using the data from the input fields) to the
   * customCommandService to create a new command on the server.
   * @return commandsChanged This event is emitted, in success and error case, as soon as the action is complete.
   */
  changeCommand() {
    this.submitted = true;
    if (this.form.valid) {

      var newCommand = this.form.value;
      newCommand.isActive = this.command.isActive;

      this.customCommandService.deleteCommand(this.command.commandId)
        .subscribe((success:boolean) => {

          if (success) {
            this.customCommandService.addCommand(newCommand)
              .subscribe((success:boolean) => {

                this.commandsChanged.emit(null);
                if (success) {
                  this.alertService.alert({type: 'alert-success', message: 'Command changed.', autoResolve: true});
                } else {
                  this.alertService.alert({type: 'alert-danger', message: 'Command could not be changed - the command was deleted.', autoResolve: true});
                }

              });
          } else {
            this.alertService.alert({type: 'alert-danger', message: 'Command could not be changed.', autoResolve: true
            });
          }

        });

    }
  }

}
