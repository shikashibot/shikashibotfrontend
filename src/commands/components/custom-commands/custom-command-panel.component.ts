import {Component, Input, EventEmitter, Output} from '@angular/core';
import {CustomCommandService} from '../../services/custom-commands.service';
import {CustomCommandFormComponent} from './custom-command-form.component';
import {AlertService} from '../../../app/services/alert.service';

@Component({
  selector: 'sb-custom-command-panel',
  moduleId: module.id,
  templateUrl: './custom-command-panel.component.html',
  styleUrls: ['./custom-command-panel.component.css'],
  providers: [CustomCommandService],
  directives: [CustomCommandFormComponent]
})

/**
 * This component includes a expandable div container. A toggle to activate /
 * deactivate the command is shown if a command is provided.
 * @param command The command object as received from the server or null.
 * @param index The index of the command in the command array received from the server or 'new-command'
 * @return commandsChanged This event is emitted if the commands have changed in any way (a command was created/deleted/changed)
 */
export class CustomCommandPanelComponent {
  @Input() command: any;
  @Input() index: any;

  @Output() commandsChanged = new EventEmitter<any>();

  constructor(private customCommandService: CustomCommandService, private alertService: AlertService) {}

  /**
   * Sets the isActive property equal to the user input and passes the command
   * object to the customCommandsService to change the command-state on the server
   * @param checkbox A object representing the checkbox ui element
   */
  setIsActive(checkbox:any) {
    this.command.isActive = checkbox.checked;

    this.customCommandService.setIsActive(this.command)
      .subscribe((success:boolean) => {
        if (!success) {
          this.command.isActive ? this.command.isActive = false : this.command.isActive = true;
          this.alertService.alert({type: 'alert-danger', message: 'Command status could not be changed.', autoResolve: true});
        }
      });
  }

  /**
   * Emits the commandsChanged event.
   * @return commandsChanged This event indicates that the commands have changed in any way.
   */
  onCommandsChanged(input:any) {
    this.commandsChanged.emit(null);
  }
}
