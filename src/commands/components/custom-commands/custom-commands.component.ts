import {Component, OnInit} from '@angular/core';
import {CustomCommandService} from '../../services/custom-commands.service';
import {CustomCommandPanelComponent} from './custom-command-panel.component';
import {AlertService} from '../../../app/services/alert.service';

@Component({
  selector: 'sb-custom-commands',
  moduleId: module.id,
  templateUrl: './custom-commands.component.html',
  styleUrls: ['./custom-commands.component.css'],
  providers: [CustomCommandService],
  directives: [CustomCommandPanelComponent]
})

/**
 * This component includes a list of custom-command-panels as well as one panel to create a new command.
 * It fetches a list of all custom commands from the server. If the commandsChanged event is fired it
 * reloads the commands.
 */
export class CustomCommandsComponent implements OnInit {

  private loadingComplete: boolean;
  private customCommands: any;

  constructor(private customCommandService: CustomCommandService, private alertService: AlertService) {}

  ngOnInit() {
    this.loadCommands();
  }

  /**
   * Uses the customCommandService to load all commands from the server.
   */
  loadCommands() {
    this.customCommandService.getCommands()
      .subscribe((data:any) => {
        if (data) {
          this.customCommands = data;
          this.loadingComplete = true;
        } else {
          this.alertService.alert({type: 'alert-danger', message: 'Could not load custom commands.', autoResolve: true});
        }
      });
  }

}
