import {Component} from '@angular/core';
import {CustomCommandsComponent} from './custom-commands/custom-commands.component';
import {DefaultCommandsComponent} from './default-commands/default-commands.component';

@Component({
  selector: 'sb-commands',
  moduleId: module.id,
  templateUrl: './commands.component.html',
  styleUrls: ['./commands.component.css'],
  directives: [DefaultCommandsComponent, CustomCommandsComponent]
})

/**
 * This component includes the DefaultCommandComponent and the CustomCommandComponent
 */
export class CommandsComponent {}
