import {Component} from '@angular/core';
import {FormListComponent} from './../../shared/components/form-list.component';

@Component({
  selector: 'sb-regulars',
  moduleId: module.id,
  templateUrl: './regulars.component.html',
  styleUrls: ['./regulars.component.css'],
  directives: [FormListComponent]
})

export class RegularsComponent {
}
