import {Injectable} from '@angular/core';
import {Http, Response, Headers, ConnectionBackend, RequestOptions} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {AuthService} from './auth.service';
import {AppRouterService} from '../../app/services/app-router.service';
import {AlertService} from '../../app/services/alert.service';

@Injectable()

/**
 * This service extends angular2/Http to centralize tasks which are the same for all http requests to the server
 * such as header preparation and error handling.
 */
export class HttpService extends Http {

  baseUrl = '<%= shikashiConfig.SHIKASHI_API_BASE_URL %>';

  constructor (connectionBackend: ConnectionBackend, requestOptions: RequestOptions, private authService: AuthService, private appRouterService: AppRouterService, private alertService: AlertService) {
    super(connectionBackend,requestOptions);
  }

  get(endpoint: string) : Observable<Response> {

    return this.req(endpoint, 'get');

  }

  post(endpoint: string, body: string) : Observable<Response> {

    return this.req(endpoint, 'post', body);

  }

  put(endpoint: string, body: string) : Observable<Response> {

    return this.req(endpoint, 'put', body);

  }

  delete(endpoint: string) : Observable<Response> {

    return this.req(endpoint, 'delete');

  }

  patch(endpoint: string, body: string) : Observable<Response> {

    return this.req(endpoint, 'patch', body);

  }

  head(endpoint: string) : Observable<Response> {

    return this.req(endpoint, 'head');

  }

  /**
   * General request method
   * @endpoint Specifies the api endpoint on top of the base URL
   * @method Specifies the HTTP method
   */
  req(endpoint: string, method: string, body?: string) {

    return Observable.create((observer:any) => {

      const credentials = this.authService.getShikashiCredentials();
      if (credentials && credentials.channelName && credentials.token) {

        // Set request auth header
        const headers = new Headers();
        headers.append('Authorization', 'Bearer ' + credentials.token);
        if (method === 'post' || method === 'put' || method === 'patch') {headers.append('Content-Type', 'application/json');}

        // Prepare URL
        var url = this.baseUrl + endpoint;
        url = url.replace('[CHANNELNAME]', credentials.channelName);

        var req: any;
        switch (method) {
          case 'get':
                req = super.get(url, { headers: headers });
                break;
          case 'post':
                req = super.post(url, body, { headers: headers });
                break;
          case 'put':
                req = super.put(url, body, { headers: headers });
                break;
          case 'delete':
                req = super.delete(url, { headers: headers });
                break;
          case 'patch':
                req = super.patch(url, body, { headers: headers });
                break;
          case 'head':
                req = super.head(url, { headers: headers });
                break;
        }

        req.subscribe(
          (data: any) => this.handleSuccess(data, observer),
          (error: any) => this.handleError(error, observer)
        );

      } else {
        this.handleAuthError();
      }

    });

  }

  /**
   * Handles successfully received responses
   * @data
   * @observer
   */
  handleSuccess(data:any, observer:any) {
    observer.next(data);
    observer.complete();
  }

  /**
   * Handles error responses. Reacts to different error codes
   * @error
   * @observer
   */
  handleError(error:any, observer:any) {
    console.log(error);
    if (error.status === 401) {
      this.handleAuthError();
    } else {
      observer.error(error);
      observer.complete();
    }
  }

  /**
   * Handles authentication errors
   */
  handleAuthError() {
    this.authService.invalidateShikashiToken()
      .subscribe( () => this.appRouterService.getRouter().navigate(['Landing']) );
    this.alertService.alert({type: 'alert-danger', message: 'Authentication error. Did you sign in on another device?', autoResolve: true});
  }

}
