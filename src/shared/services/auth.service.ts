import {Injectable, EventEmitter} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';

@Injectable()

/**
 * This service manages the user-credentials - stores them in local storage and communicates to the server.
 * This service uses angular2/http instead of httpService since it handles authorization by itself
 */
export class AuthService {

  constructor (private http: Http) {}

  /**
   * Stores the credentials in local storage.
   * @params channelName
   * @params token
   */
  setShikashiCredentials(channelName: string, token: string) {
    var credentials = { channelName: channelName, token: token};
    localStorage.setItem('shikashiCredentials', JSON.stringify(credentials));
  }

  /**
   * Reads the credentials from local storage
   */
  getShikashiCredentials() {
    return JSON.parse(localStorage.getItem('shikashiCredentials'));
  }

  /**
   * Deletes the credentials from local storage
   */
  deleteShikashiCredentials() {
    localStorage.removeItem('shikashiCredentials');
  }

  /**
   * Sends the twitchtoken to the shikashi server and retrieves a shikashi token in exchange.
   * @param twitchToken
   */
  obtainAndStoreShikashiToken(twitchToken: string) {
    var obtainTokenEvent = new EventEmitter();
    const payload = { token: twitchToken };

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post('<%= shikashiConfig.SHIKASHI_API_BASE_URL %>' + 'auth', JSON.stringify(payload), { headers: headers })
      .map(response => response.json())
      .subscribe(
        data => {
          this.setShikashiCredentials(data.channelName, data.token);
          console.log('Successfully obtained shikashi token.');
          obtainTokenEvent.emit(true);
        },
        error => {
          console.log('Could not obtain shikashi token. Make sure to provide a valid twitch token.');
          console.log(error);
          obtainTokenEvent.emit(false);
        }
      );
    return obtainTokenEvent;
  }

  /**
   * Sends the shikashitoken to the server for validation
   * @return validateTokenEvent This event is emitted as soon as the server respnded. Is either true (valid Token)
   * or false (token is invalid)
   */
  validateShikashiToken() {
    var validateTokenEvent = new EventEmitter();
    const credentials = this.getShikashiCredentials();

    if (credentials && credentials.channelName && credentials.token) {
      const headers = new Headers();
      headers.append('Authorization', 'Bearer ' + credentials.token);

      this.http.get('<%= shikashiConfig.SHIKASHI_API_BASE_URL %>' + 'auth', { headers: headers })
        .subscribe(
          data => {
            console.log('Successfully validated shikashi token.');
            validateTokenEvent.emit(true);
          },
          error => {
            this.deleteShikashiCredentials();
            console.log('Could not validate shikashi token.');
            console.log(error);
            validateTokenEvent.emit(false);
          }
        );
    } else {
      validateTokenEvent.emit(false);
    }

    return validateTokenEvent;
  }

  /**
   * Invalidates the shikashitoken on the server and deletes the token from local storage
   * @return invalidateTokenEvent This event is emitted as soon as the server respnded.
   */
  invalidateShikashiToken() {
    const credentials = this.getShikashiCredentials();
    var invalidateTokenEvent: EventEmitter<any> = new EventEmitter();

    if (credentials && credentials.channelName && credentials.token) {
      const headers = new Headers();
      headers.append('Authorization', 'Bearer ' + credentials.token);

      this.http.delete('<%= shikashiConfig.SHIKASHI_API_BASE_URL %>' + 'auth', { headers: headers })
        .subscribe(
          data => {
            this.deleteShikashiCredentials();
            console.log('Successfully invalidated shikashi token.');
            invalidateTokenEvent.emit(null);
          },
          error => {
            this.deleteShikashiCredentials();
            console.log('Could not invalidate shikashi token. Token was deleted from local storage.');
            console.log(error);
            invalidateTokenEvent.emit(null);
          }
        );
    } else {
      setTimeout(() => invalidateTokenEvent.emit(null), 0);
    }

    return invalidateTokenEvent;
  }

}
