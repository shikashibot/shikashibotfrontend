import {EventEmitter, Injectable} from '@angular/core';
import {HttpService} from './http.service';

@Injectable()

/**
 * This service uses the HttpService to communicate to the server and modify list of string-items (Words, Domains or Regulars)
 */
export class FormListService {

  private identifier: string;
  private listEndpoint: string;

  constructor(private httpService: HttpService) {}

  /**
   * Sets the identifier and determines the urlSuffix (the REST-endpoint)
   * @param identifier
   */
  setIdentifier(identifier: string) {
    this.identifier = identifier;
    switch (identifier) {
      case 'Word':
            this.listEndpoint = 'channels/[CHANNELNAME]/bannedWords';
            break;
      case 'Domain':
            this.listEndpoint = 'channels/[CHANNELNAME]/whitelistedDomains';
            break;
      case 'Regulars':
            this.listEndpoint = 'channels/[CHANNELNAME]/regulars';
            break;
    }
  }

  /**
   * Gets all items.
   * @return getItemsEvent This event is fired as soon as a response is received. Carries either the list of items
   * or null (in case of failure)
   */
  getItems() {
    var getItemsEvent: EventEmitter<any> = new EventEmitter();

    this.httpService.get(this.listEndpoint)
      .map((response) => response.json())
      .subscribe(
        data => {
          console.log('Successfully received list data (' + this.identifier + ')');
          getItemsEvent.emit(data);
        },
        error => {
          console.log('Could not load list data (' + this.identifier + ')');
          getItemsEvent.emit(null);
        }
      );

    return getItemsEvent;
  }

  /**
   * Adds an item.
   * @param item
   * @return addItemEvent This event is fired as soon as a response is received. Is either true (in case of success) or
   * false (in case of failure).
   */
  addItem(item: string) {
    var payload = this.prepareRequest(item);
    var addItemEvent = new EventEmitter();

    this.httpService.put(this.listEndpoint, JSON.stringify(payload))
      .subscribe(
        data => {
          console.log('Successfully added new item (' + this.identifier + ': ' + item +')');
          addItemEvent.emit(true);
        },
        error => {
          console.log('Could not add new item (' + this.identifier + ': ' + item +')');
          addItemEvent.emit(false);
        }
      );

    return addItemEvent;
  }

  /**
   * Deletes an item.
   * @param item
   * @return deleteItemEvent This event is fired as soon as a response is received. Is either true (in case of success) or
   * false (in case of failure).
   */
  deleteItem(item: string) {
    var deleteItemEvent = new EventEmitter();

    this.httpService.delete(this.listEndpoint + '/' + item)
      .subscribe(
        data => {
          console.log('Successfully deleted item (' + this.identifier + ': ' + item +')');
          deleteItemEvent.emit(true);
        },
        error => {
          console.log('Could not delete item (' + this.identifier + ': ' + item +')');
          deleteItemEvent.emit(false);
        }
      );

    return deleteItemEvent;
  }

  /**
   * Creates objects from the strings (items) as they are expected from the server.
   * @param item A string.
   * @return object A object or null if the identifier is unknown.
   */
  private prepareRequest(item: string):any {
    switch(this.identifier) {
      case 'Word':
            return {Word: item};
      case 'Domain':
            return {Domain: item};
      case 'Regulars':
            return {Username: item};
    }
    return null;
  }

}
