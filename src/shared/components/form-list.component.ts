import {Component, OnInit, Input, AfterViewInit} from '@angular/core';
import {FormListService} from '../services/form-list.service';
import {AlertService} from '../../app/services/alert.service';
declare var jQuery: any;

@Component({
  selector: 'sb-form-list',
  moduleId: module.id,
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.css'],
  providers: [FormListService]
})

/**
 * This component includes a editable list of strings.
 */
export class FormListComponent implements OnInit, AfterViewInit {

  @Input() type: any;

  items: any;
  submitted: boolean;
  identifier: any;
  currentFormValue: string;
  loadingComplete: boolean;

  constructor(private filterFormListService: FormListService, private alertService: AlertService) {}

  ngOnInit():any {
    this.identifier = this.type;
    this.filterFormListService.setIdentifier(this.identifier);
    this.loadItems();
  }

  ngAfterViewInit() {
    // Do not show the browser error message
    jQuery('input').on('invalid', function(e:any) {
      e.preventDefault();
    });
  }

  /**
   * Uses the formListService to load all items from the server.
   */
  loadItems() {
    this.filterFormListService.getItems()
      .subscribe((data:any) => {
          if (data) {
             this.items = data;
          }

          this.loadingComplete = true;
        });
  }

  /**
   * Passes a new item (using the data from the input field) to the
   * formListService to create a new item on the server.
   * @param item The item from the input field.
   */
  addItem(item:any) {
    this.submitted = true;
    if (item.valid) {
      this.filterFormListService.addItem(item.value)
        .subscribe((success:boolean) => {
          if(success) {
            this.loadItems();
            this.alertService.alert({type: 'alert-success', message: this.identifier + ' added.', autoResolve: true});
            this.submitted = false;
            this.currentFormValue = '';
          } else {
            this.alertService.alert({type: 'alert-danger', message: this.identifier + ' could not be added.', autoResolve: true});
          }
        });
    }
  }

  /**
   * Passes a item to the formListService to delete the item on the server.
   * @param item
   */
  deleteItem(item:string) {
    this.filterFormListService.deleteItem(item)
      .subscribe((success:boolean) => {
        if(success) {
          this.loadItems();
          this.alertService.alert({type: 'alert-success', message: this.identifier + ' deleted.', autoResolve: true});
        } else {
          this.alertService.alert({type: 'alert-danger', message: this.identifier + ' could not be deleted.', autoResolve: true});
        }
      });
  }

}
