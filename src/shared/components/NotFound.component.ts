import {Component} from '@angular/core';

@Component({
  selector: 'sb-not-found',
  moduleId: module.id,
  templateUrl: './not-found.component.html'
})

/**
 * The 404 page
 */
export class NotFoundComponent {}
