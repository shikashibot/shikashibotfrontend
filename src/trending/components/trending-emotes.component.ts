import {Component, OnInit, OnDestroy} from '@angular/core';
import {TrendingService} from "../services/trending.service";
import {AlertService} from "../../app/services/alert.service";

@Component({
  selector: 'sb-trending-emotes',
  moduleId: module.id,
  templateUrl: './trending-emotes.component.html',
  styleUrls: ['./trending-items.component.css']
})

export class TrendingEmotesComponent implements OnInit, OnDestroy {

  private loadingComplete: boolean;
  private trendingEmotes: any;
  private componentActive: boolean = true;

  constructor(private trendingService: TrendingService, private alertService: AlertService) {}

  ngOnInit() {
    this.loadTrendingEmotes();
  }

  ngOnDestroy():any {
    this.componentActive = false;
  }

  /**
   * Uses the trendingService to load the trending emotes from the server.
   */
  loadTrendingEmotes() {
    this.trendingService.getTrendingEmotes()
      .subscribe((data:any) => {
        if (data) {
          this.trendingEmotes = data;
          this.loadingComplete = true;
        } else {
          this.alertService.alert({type: 'alert-danger', message: 'Could not load trending emotes.', autoResolve: true});
        }
      });

    setTimeout(() => {
      if (this.componentActive) {
        this.loadTrendingEmotes();  
      }
    }, 5000);
  }

}
