import {Component, OnInit, OnDestroy} from '@angular/core';
import {AlertService} from "../../app/services/alert.service";
import {TrendingService} from "../services/trending.service";

@Component({
  selector: 'sb-trending-messages',
  moduleId: module.id,
  templateUrl: './trending-messages.component.html',
  styleUrls: ['./trending-items.component.css']
})

export class TrendingMessagesComponent implements OnInit, OnDestroy {

  private loadingComplete: boolean;
  private trendingMessages: any;
  private componentActive: boolean = true;

  constructor(private trendingService: TrendingService, private alertService: AlertService) {}

  ngOnInit() {
    this.loadTrendingMessages();
  }

  ngOnDestroy():any {
    this.componentActive = false;
  }
  
  /**
    * Uses the trendingService to load the trending messages from the server.
    */
  loadTrendingMessages() {
    this.trendingService.getTrendingMessages()
      .subscribe((data:any) => {
        if (data) {
          this.trendingMessages = data;
          this.loadingComplete = true;
        } else {
          this.alertService.alert({type: 'alert-danger', message: 'Could not load trending messages.', autoResolve: true});
        }
      });

    setTimeout(() => {
      if (this.componentActive) {
        this.loadTrendingMessages();
      }
    }, 5000);
  }

}
