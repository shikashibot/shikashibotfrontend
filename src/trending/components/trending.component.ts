import {Component} from '@angular/core';
import {TrendingService} from '../services/trending.service';
import {TrendingMessagesComponent} from './trending-messages.component';
import {TrendingEmotesComponent} from './trending-emotes.component';

@Component({
  selector: 'sb-trending',
  moduleId: module.id,
  templateUrl: './trending.component.html',
  styleUrls: ['./trending.component.css'],
  directives: [TrendingMessagesComponent, TrendingEmotesComponent],
  providers: [TrendingService]
})

export class TrendingComponent {

  activePage = 'messages';

  setActivePage(activePage: string) {
    this.activePage = activePage;
  }
}
