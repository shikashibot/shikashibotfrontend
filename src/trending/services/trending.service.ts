import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/Rx';
import {HttpService} from '../../shared/services/http.service';

@Injectable()

/**
 * This service uses the HttpService to receive the trending statistics from the server
 */
export class TrendingService {

  trendingEndpoint = 'channels/[CHANNELNAME]/statistics/';

  constructor (private httpService: HttpService) {}

  /**
   * Gets the trending messages.
   * @return getTrendingMessages This event is fired as soon as a response is received. Carries either the trending messages
   * or null (in case of failure)
   */
  getTrendingMessages() {
    var getTrendingMessages = new EventEmitter();

    this.httpService.get(this.trendingEndpoint + 'messages')
      .map(response => response.json())
      .subscribe(
        data => {
          console.log('Successfully received trending messages.');
          getTrendingMessages.emit(data);
        },
        error => {
          console.log('Could not load trending messages.');
          getTrendingMessages.emit(null);
        }
      );

    return getTrendingMessages;
  }

  /**
   * Gets the trending emotes.
   * @return getTrendingEmotes This event is fired as soon as a response is received. Carries either the trending emotes
   * or null (in case of failure)
   */
  getTrendingEmotes() {
    var getTrendingEmotes = new EventEmitter();

    this.httpService.get(this.trendingEndpoint + 'emotes')
      .map(response => response.json())
      .subscribe(
        data => {
          console.log('Successfully received weekly statistics.');
          getTrendingEmotes.emit(data);
        },
        error => {
          console.log('Could not load weekly statistics.');
          getTrendingEmotes.emit(null);
        }
      );

    return getTrendingEmotes;
  }

}
