import {Component, Input, OnInit, Output, AfterViewInit} from '@angular/core';
import {FormBuilder, ControlGroup, Validators, Control} from '@angular/common';
import {EventEmitter} from '@angular/core';
import {AnnouncementsService} from '../services/announcements.services';
import {AlertService} from '../../app/services/alert.service';
declare var jQuery: any;

@Component({
  selector: 'sb-announcement-form',
  moduleId: module.id,
  templateUrl: './announcement-form.component.html',
  styleUrls: ['./announcement-form.component.css']
})

/**
 * This component includes the input fields for the announcement message
 * and time interval. It can either show a "change announcement" and a
 * "delete announcement" button (for existing announcements) or a "create
 * announcement" button (for creating a new announcement)
 * @param announcement The announcement object as received from the server or null.
 * @param index The index of the announcement in announcement array received from the server or 'new-announcement'
 * @return announcementsChanged This event is emitted if the announcements have changed in any way (a announcement was created/deleted/changed)
*/
export class AnnouncementFormComponent implements OnInit, AfterViewInit {
  @Input() announcement: any;
  @Input() index: any;

  @Output() announcementsChanged = new EventEmitter<any>();

  form: ControlGroup;
  submitted = false;

  private defaultInterval: number = 30;

  constructor(private formBuilder: FormBuilder, private announcementsService: AnnouncementsService, private alertService: AlertService) {}

  ngOnInit():any {
    this.form = this.formBuilder.group({
      'message': [this.announcement ? this.announcement.message : '', Validators.required],
      'secondsInterval': [this.announcement ? this.announcement.secondsInterval : this.defaultInterval, this.secondsIntervalValid]
    });
  }

  ngAfterViewInit() {
    // Activate Bootstrap tooltips
    jQuery('[data-toggle="tooltip"]').tooltip();
  }

  /**
   * Passes the data from the input fields to the announcementService to create
   * a announcement on the server
   * @return announcementsChanged This event is emitted if the announcement was successfully added
   */
  addAnnouncement() {
    this.submitted = true;
    if(this.form.valid) {

      var newAnnouncement = this.form.value;
      newAnnouncement.id = null;
      newAnnouncement.isActive = true;

      this.announcementsService.addAnnouncement(newAnnouncement)
        .subscribe((success:boolean) => {
          if(success) {
            this.announcementsChanged.emit(null);
            this.alertService.alert({type: 'alert-success', message: 'Announcement added.', autoResolve: true});
            this.submitted = false;

            (<Control> this.form.controls['message']).updateValue('', true);
            (<Control> this.form.controls['secondsInterval']).updateValue(this.defaultInterval, true);
          } else {
            this.alertService.alert({type: 'alert-danger', message: 'Announcement could not be added.', autoResolve: true});
          }
        });
    }
  }

  /**
   * Passes the announcementId to the announcementService to delete the announcement
   * on the server
   * @return announcementsChanged This event is emitted if the announcement was successfully deleted
  */
  deleteAnnouncement() {
    this.announcementsService.deleteAnnouncement(this.announcement.id)
      .subscribe((success:boolean) => {
        if(success) {
          this.announcementsChanged.emit(null);
          this.alertService.alert({type: 'alert-success', message: 'Announcemnet deleted.', autoResolve: true});
        } else {
          this.alertService.alert({type: 'alert-danger', message: 'Announcement could not be deleted.', autoResolve: true});
        }
      });
  }

  /**
   * Passes a new announcement object (using the data from the input fields) to the
   * announcementService to change the announcement on the server.
   * @return announcementsChanged This event is emitted when the response is received (in both cases: success or failure)
  */
  changeAnnouncement() {
    this.submitted = true;
    if (this.form.valid) {

      var newAnnouncement = this.form.value;
      newAnnouncement.id = this.announcement.id;
      newAnnouncement.isActive = this.announcement.isActive;

      this.announcementsService.changeAnnouncement(newAnnouncement)
        .subscribe((success:boolean) => {
          if (success) {
            this.announcementsChanged.emit(null);
            this.alertService.alert({type: 'alert-success', message: 'Announcement changed.', autoResolve: true});
          } else {
            this.announcementsChanged.emit(null);
            this.alertService.alert({type: 'alert-danger', message: 'Announcement could not be changed.', autoResolve: true});
          }
        });

    }
  }

  /**
   * Checks if the user-input for the interval is valid
   * @param control A angular2 control object representing the input field
   * @return errorObject A object to indicate that the validation was unsuccessful (returns nothing if the validation was successful)
  */
  secondsIntervalValid(control: Control): any {
    if (isNaN(control.value)) {
      return {NaN: true};
    } else if (control.value < 30) {
      return {lessThanThirty: true};
    }
  }

}
