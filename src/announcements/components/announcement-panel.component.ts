import {Component, Input, EventEmitter, Output} from '@angular/core';
import {AnnouncementsService} from '../services/announcements.services';
import {AlertService} from '../../app/services/alert.service';
import {EllipsisPipe} from '../pipes/elipsis.pipe';
import {AnnouncementFormComponent} from './announcement-form.component';

@Component({
  selector: 'sb-announcement-panel',
  moduleId: module.id,
  templateUrl: './announcement-panel.component.html',
  styleUrls: ['./announcement-panel.component.css'],
  directives: [AnnouncementFormComponent],
  pipes: [EllipsisPipe]
})

/**
 * This component includes a expandable div container. A toggle to activate /
 * deactivate the announcement is shown if a announcement is provided.
 * @param announcement The announcement object as received from the server or null.
 * @param index The index of the announcement in announcement array received from the server or 'new-announcement'
 * @return announcementsChanged This event is emitted if the announcements have changed in any way (a announcement was created/deleted/changed)
*/
export class AnnouncementPanelComponent {
  @Input() announcement: any;
  @Input() index: any;

  @Output() announcementsChanged = new EventEmitter<any>();

  constructor(private announcementService: AnnouncementsService, private alertService: AlertService) {}

  /**
   * Sets the isActive property equal to the user input and passes the announcement
   * object to the announcementService to change the announcement-state on the server
   * @param checkbox A object representing the checkbox ui element
  */
  setIsActive(checkbox:any) {
    this.announcement.isActive = checkbox.checked;

    this.announcementService.changeAnnouncement(this.announcement)
      .subscribe((success:boolean) => {
        if (!success) {
          this.announcement.isActive ? this.announcement.isActive = false : this.announcement.isActive = true;
          this.alertService.alert({type: 'alert-danger', message: 'Announcement state could not be changed.', autoResolve: true});
        }
      });
  }

  /**
   * Emits the announcementChanged event.
   * @return announcementsChanged This event indicates that the announcements have changed in any way.
   */
  onAnnouncementsChanged() {
    this.announcementsChanged.emit(null);
  }
}
