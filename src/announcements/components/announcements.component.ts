import {Component, OnInit} from '@angular/core';
import {AnnouncementsService} from '../services/announcements.services';
import {AnnouncementPanelComponent} from './announcement-panel.component';
import {AlertService} from '../../app/services/alert.service';

@Component({
  selector: 'sb-announcements',
  moduleId: module.id,
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.css'],
  providers: [AnnouncementsService] ,
  directives: [AnnouncementPanelComponent]
})

/**
 * This component includes a list of announcement-panels as well as one panel to create a new announcement.
 * It fetches a list of all announcements from the server. If the announcementsChanged event is fired it
 * reloads the announcements.
*/
export class AnnouncementsComponent implements OnInit {

  private loadingComplete: boolean;
  private announcements: any;

  constructor(private announcementsService: AnnouncementsService, private alertService: AlertService) {}

  ngOnInit() {
    this.loadAnnouncements();
  }

  /**
   * Uses the announcementsService to load all announcements from the server.
   */
  loadAnnouncements() {
    this.announcementsService.getAnnouncements()
      .subscribe((data:any) => {
        if (data) {
          this.announcements = data;
          this.loadingComplete = true;
        } else {
          this.alertService.alert({type: 'alert-danger', message: 'Could not load announcements.', autoResolve: true});
        }
      });
  }

}
