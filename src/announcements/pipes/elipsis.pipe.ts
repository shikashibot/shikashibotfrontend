// Use this pipe in the panel title:  "... announcement.message | ellipsis:20 ..."
// once https://github.com/angular/angular/issues/5169 is resolved

import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'ellipsis'
})

/**
 * This pipe cuts a string to a certain length and adds three periods to it
 * @param val The desired length of the string
 */
export class EllipsisPipe implements PipeTransform {
  transform(val: any, args: any) {
    if (args[0] === -1) {
      return val;
    }

    if (val.length > args[0]) {
      return val.substring(0, args[0]) + '...';
    } else {
      return val;
    }
  }
}
