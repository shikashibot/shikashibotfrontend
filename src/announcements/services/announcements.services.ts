import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/Rx';
import {HttpService} from '../../shared/services/http.service';

@Injectable()

/**
 * This service uses the HttpService to communicate to the server and modify the announcements of a channel
 */
export class AnnouncementsService {

  announcementsEndpoint = 'channels/[CHANNELNAME]/announcements';

  constructor (private httpService: HttpService) {}

  /**
   * Gets all announcements of a channel.
   * @return getAnnouncementsEvent This event is fired as soon as a response is received. Carries either the list of announcements
   * or null (in case of failure)
   */
  getAnnouncements() {
    var getAnnouncementsEvent = new EventEmitter();

    this.httpService.get(this.announcementsEndpoint)
      .map(response => response.json())
      .subscribe(
        data => {
          console.log('Successfully received announcements.');
          getAnnouncementsEvent.emit(data);
        },
        error => {
          console.log('Could not load announcements.');
          getAnnouncementsEvent.emit(null);
        }
      );

    return getAnnouncementsEvent;
  }

  /**
   * Adds an announcement for a channel.
   * @param newAnnouncement A object holding the data for the new announcement: { id: number, secondsInterval: number, message: string, isActive: boolean }
   * @return addAnnouncementEvent This event is fired as soon as a response is received. Is either true (in case of success) or
   * false (in case of failure)
   */
  addAnnouncement(newAnnouncement:{ id: number, secondsInterval: number, message: string, isActive: boolean }) {
    var addAnnouncementEvent = new EventEmitter();

    this.httpService.post(this.announcementsEndpoint, JSON.stringify(newAnnouncement))
      .subscribe(
        data => {
          console.log('Successfully added new announcement.');
          addAnnouncementEvent.emit(true);
        },
        error => {
          console.log('Could not add new announcement.');
          addAnnouncementEvent.emit(false);
        }
      );

    return addAnnouncementEvent;
  }

  /**
   * Deletes an announcement for a channel.
   * @param announcementId The id of the announcement
   * @return deleteAnnouncementEvent This event is fired as soon as a response is received. Is either true (in case of success) or
   * false (in case of failure)
   */
  deleteAnnouncement(announcementId: number) {
    var deleteAnnouncementEvent = new EventEmitter();

    this.httpService.delete(this.announcementsEndpoint + '/'+ announcementId)
      .subscribe(
        data => {
          console.log('Successfully deleted announcement.');
          deleteAnnouncementEvent.emit(true);
        },
        error => {
          console.log('Could not delete announcement.');
          deleteAnnouncementEvent.emit(false);
        }
      );

    return deleteAnnouncementEvent;
  }

  /**
   * Changes a announcement for a channel.
   * @param newAnnouncement A object holding the data for the new announcement: { id: number, secondsInterval: number, message: string, isActive: boolean }
   * @return changeAnnouncementEvent This event is fired as soon as a response is received. Is either true (in case of success) or
   * false (in case of failure)
   */
  changeAnnouncement(newAnnouncement:{ id: number, secondsInterval: number, message: string, isActive: boolean }) {
    var changeAnnouncementEvent = new EventEmitter();

    this.httpService.put(this.announcementsEndpoint + '/' + newAnnouncement.id, JSON.stringify(newAnnouncement))
      .subscribe(
        data => {
          console.log('Successfully changed announcement (' + newAnnouncement.id + ')');
          changeAnnouncementEvent.emit(true);
        },
        error => {
          console.log('Could not change announcement (' + newAnnouncement.id + ')');
          changeAnnouncementEvent.emit(false);
        }
      );

    return changeAnnouncementEvent;
  }

}
