import {provide, enableProdMode} from '@angular/core';
import {bootstrap} from '@angular/platform-browser-dynamic';
import {ROUTER_PROVIDERS} from '@angular/router-deprecated';
import {APP_BASE_HREF} from '@angular/common';
import {AppComponent} from './app/components/app.component';
import {HTTP_PROVIDERS, RequestOptions, XHRBackend} from '@angular/http';
import {AuthService} from './shared/services/auth.service';
import {AlertService} from './app/services/alert.service';
import {AppRouterService} from './app/services/app-router.service';
import {LandingService} from './landing/services/landing.service';
import {HttpService} from './shared/services/http.service';

if ('<%= ENV %>' === 'prod') { enableProdMode(); }

bootstrap(AppComponent, [
  ROUTER_PROVIDERS,
  provide(APP_BASE_HREF, { useValue: '<%= APP_BASE %>' }),
  HTTP_PROVIDERS,
  AuthService,
  AlertService,
  AppRouterService,
  LandingService,
  provide(HttpService, {
    useFactory: (connectionBackend: XHRBackend, requestOptions: RequestOptions, authService: AuthService, appRouterService: AppRouterService, alertService: AlertService) => new HttpService(connectionBackend, requestOptions, authService, appRouterService, alertService),
    deps: [XHRBackend, RequestOptions, AuthService, AppRouterService, AlertService]
  })
]);
