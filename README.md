# README #

This is the repo for the shikashibot web application which is realized with typescript using the the angular 2 framework. The app provides a simple UI to configure the bot. It communicates with the shikashi-backend over its REST interface.

### Requirements ###

* Make sure you have the [shikashibot-backend project](https://bitbucket.org/shikashibot/shikashibot) up and running
* Make sure you have [node.js / npm](https://nodejs.org/en/) installed

### How do I get set up? ###

* Clone this repo
* Make a copy of the file `tools/config/shikashi.config.sample.ts` and name it `tools/config/shikashi.config.ts`
* Open the file `tools/config/shikashi.config.ts` and set the value of `TWITCH_API_CLIENT_ID` to the client id of your twitch app
* Also set the value of `SHIKASHI_API_BASE_URL` if you're running the shikashibot-backend on another host or port than localhost:5000
* Also set the value of `TWITCH_API_REDIRECT_URI` if you're serving the web application from another host or port than localhost:5555
* Run `npm install` inside the root folder of the project
* Run `npm start` inside the root folder to start the project
